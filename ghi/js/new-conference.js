window.addEventListener('DOMContentLoaded',  async()=> {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
    
    if(response.ok){
        const data= await response.json();
        console.log(data)

        const selectLocation = document.getElementById('location');
        for (let locate of data.locations){
            const option = document.createElement("option")
            option.value = locate.id;
            option.innerHTML = locate.name;
            selectLocation.append(option)
        }
        
    }
});
