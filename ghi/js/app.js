function createCard(name, description, pictureUrl,formattedStarts, formattedEnds, location) {
    return `
    <div class="col">
      <div class="card mb-5 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class="text-muted">${formattedStarts} - ${formattedEnds}</small>
        </div>
      </div>
    </div>
    `;
  }
function createError(error){
    return `
    <div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
    </div>`
}
window.addEventListener('DOMContentLoaded', async() => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if(!response.ok) {

        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const dateStarts= new Date(details.conference.starts);
                    const formattedStarts = `${dateStarts.getMonth()}/${dateStarts.getDay()}/${dateStarts.getFullYear()}`;
                    const dateEnds = new Date(details.conference.ends);
                    const formattedEnds = `${dateEnds.getMonth()}/${dateEnds.getDay()}/${dateEnds.getFullYear()}`;
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl,formattedStarts, formattedEnds, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                    // console.log(details)
                }else {
                    throw new Error('Response not ok');
                }
            }
        }
    }catch (e) {
        const html = createError(e);
        const column = document.querySelector('.body');
        column.innerHTML += html;
        // console.error(e);
        // console.log('error',error);
    }
});
