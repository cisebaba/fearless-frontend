// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload');
if (payloadCookie) {
  // // Convert the encoded payload from base64 to normal string
  const encodedPayload = JSON.parse(payloadCookie.value);
  const decodedPayload = atob(encodedPayload);

  // // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);

  const canAddConference = payload.user.perms.includes("events.add_conference");
  const canAddLocation = payload.user.perms.includes("events.add_location");

  if (canAddConference) {
    const conferenceLinkTag = document.getElementById('new_conference_nav_link');
    conferenceLinkTag.classList.remove("d-none");
  }

  if (canAddLocation) {
    const locationLinkTag = document.getElementById('new_location_nav_link');
    locationLinkTag.classList.remove("d-none");
  }



  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link

}